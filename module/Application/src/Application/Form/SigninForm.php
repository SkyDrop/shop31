<?php
/**
 * Created by PhpStorm.
 * User: ernest
 * Date: 04.06.15
 * Time: 12:04
 */

namespace Application\Form;
use Zend\Form\Factory;

class SigninForm extends  Form{

   public function _contstruct(){
       $factory = new Factory();
       $form    = $factory->createForm(array(
           'hydrator' => 'Zend\Stdlib\Hydrator\ArraySerializable',
           'elements' => array(
               array(
                   'spec' => array(
                       'name' => 'name',
                       'options' => array(
                           'label' => 'Your name',
                       ),
                       'attributes' => array(
                           'type'  => 'text'
                       ),
                   )
               ),
               array(
                   'spec' => array(
                       'type' => 'Zend\Form\Element\Email',
                       'name' => 'email',
                       'options' => array(
                           'label' => 'Your email address',
                       )
                   ),
               ),
               array(
                   'spec' => array(
                       'name' => 'subject',
                       'options' => array(
                           'label' => 'Subject',
                       ),
                       'attributes' => array(
                           'type'  => 'text',
                       ),
                   ),
               ),
               array(
                   'spec' => array(
                       'type' => 'Zend\Form\Element\Textarea',
                       'name' => 'message',
                       'options' => array(
                           'label' => 'Message',
                       )
                   ),
               ),
               array(
                   'spec' => array(
                       'type' => 'Zend\Form\Element\Captcha',
                       'name' => 'captcha',
                       'options' => array(
                           'label' => 'Please verify you are human.',
                           'captcha' => array(
                               'class' => 'Dumb',
                           ),
                       ),
                   ),
               ),
               array(
                   'spec' => array(
                       'type' => 'Zend\Form\Element\Csrf',
                       'name' => 'security',
                   ),
               ),
               array(
                   'spec' => array(
                       'name' => 'send',
                       'attributes' => array(
                           'type'  => 'submit',
                           'value' => 'Submit',
                       ),
                   ),
               ),
           ),

           'input_filter' => array(
               /* ... */
           ),
       ));
            }
}