<?php
namespace Reg\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\ControllerManager;
use Zend\View\Model\ViewModel;
use Reg\Model\Reg;
use Reg\Form\RegForm;

class RegController extends AbstractActionController
{
    protected $regTable;

    public function getRegTable()
    {
        if (!$this->regTable) {
            $sm = $this->getServiceLocator();
            $this->regTable = $sm->get('Reg\Model\RegTable');

        }
        return $this->regTable;
    }

    public function indexAction()
    {

        $form = new RegForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $reg = new Reg();
            $form->setInputFilter($reg->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $reg->exchangeArray($form->getData());
                $this->getRegTable()->saveUser($reg);
                return $this->redirect()->toRoute('reg');
            }
        }
        return array('form' => $form);
    }

    public function loginAction()
    {

        $form = new RegForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $reg = new Reg();
            $form->setInputFilter($reg->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $reg->exchangeArray($form->getData());
                $this->getRegTable()->saveUser($reg);
                return $this->redirect()->toRoute('reg');
            }
        }
        return array('form' => $form);
    }

}