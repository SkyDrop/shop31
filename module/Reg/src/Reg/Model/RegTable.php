<?php
/**
 * Created by PhpStorm.
 * User: ernest
 * Date: 04.06.15
 * Time: 13:26
 */

namespace Reg\Model;
use Zend\Db\TableGateway\TableGateway;

class RegTable {
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
//        die('regtable');
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getReg($id)
    {
       //<--------НЕ ДОХОДИТ
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveUser(Reg $user)
    {
            if($user->password==$user->password2) {
                $user->password = md5($user->password);
                $data = array(
                    'user' => $user->user,
                    'password' => $user->password,
                    'email' => $user->email,
                );

                $id = (int)$user->id;
                if ($id == 0) {
                    $this->tableGateway->insert($data);
                } else {
                    if ($this->getReg($id)) {
                        $this->tableGateway->update($data, array('id' => $id));
                    } else {
                        throw new \Exception('User id does not exist');
                    }
                }
            }
    }

}