<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Reg\Controller\Reg' => 'Reg\Controller\RegController',
        ),
    ),

    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'reg' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/reg[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Reg\Controller\Reg',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            'reg' => __DIR__ . '/../view',
        ),
    ),
);



