<?php
namespace Product;

use Product\Model\OrdersTable;
use Product\Model\Orders;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Product\Model\Product;
use Product\Model\ProductTable;
use Zend\ModuleManager\Feature\ServiceProviderInterface;

class Module implements  ServiceProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'ProductModelProductTable' => function ($sm) {
                    $tableGateway = $sm->get('ProductTableGateway');
                    $table = new ProductTable($tableGateway);
                    return $table;
                },
                'ProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ZendDbAdapterAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new product());
                    return new TableGateway('Product', $dbAdapter, null, $resultSetPrototype);
                },
                'ProductModelOrdersTable' => function ($sm) {
                    $tableGateway = $sm->get('OrdersTableGateway');
                    $table = new OrdersTable($tableGateway);
                    return $table;
                },
                'OrdersTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('ZendDbAdapterAdapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new orders());
                    return new TableGateway('Orders', $dbAdapter, null, $resultSetPrototype);
                },
            ),

        );
    }
}