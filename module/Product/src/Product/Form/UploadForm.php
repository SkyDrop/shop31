<?php
namespace Product\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class UploadForm extends Form
{
    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->add(array(
            'name' => 'id',
            'type' => 'text',
            'options' => array(
                'label' => 'Id',
            ),
        ));
        $this->addElements();
    }

    public function addElements()
    {
        $file = new Element\File('image-file');
        $file->setLabel('Image Upload')->setAttribute('id', 'image-file');
        $this->add($file);
    }
}