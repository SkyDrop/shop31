<?php
namespace Product\Form;
use Product\Controller\Plugin\Basket;
use Zend\Form\Form;
use Zend\Form\Element;
class OrdersForm extends Form
{
    public function __construct($name = null)
    {

        parent::__construct('orders');

        $this->add(array(
            'name' => 'id',
            'type' => 'text',
            'options' => array(
                'label' => 'Id',
            ),
        ));
        $this->add(array(
            'name' => 'userId',
            'type' => 'Text',
            'options' => array(
                'label' => 'userId',
            ),
        ));
        $this->add(array(
            'name' => 'date',
            'type' => 'Text',
            'options' => array(
                'label' => 'date',
            ),
        ));
        $this->add(array(
            'name' => 'status',
            'type' => 'Text',
            'options' => array(
                'label' => 'status',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
            ),
        ));

        $basket = new Basket();
        $selectProduct =$basket->ordersProduct();
        $select = new Element\Select('productId');
        $select->setLabel('Which product you want to buy?');

        $select->setValueOptions($selectProduct);


        $this->add($select);

    }
}