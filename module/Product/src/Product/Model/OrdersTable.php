<?php
namespace Product\Model;
use Zend\Db\TableGateway\TableGateway;

class OrdersTable
{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    public function getOrders($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    public function saveOrders(orders $orders)
    {
        $data = array(
            'userId' => $orders->userId,
            'date' => $orders->date,
            'productId'=> $orders->productId,
            'status' => $orders->status,
        );

        $id =0;
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getOrders($id)) {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
}