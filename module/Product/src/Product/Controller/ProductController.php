<?php
namespace Product\Controller;

use Product\Form\UploadForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Product\Model\Product;
use Product\Model\Orders;
use Product\Form\ProductForm;
use Product\Form;
use Product\Form\OrdersForm;
use Product\Model\ProductTable;
use Product\Model\OrdersTable;
use Product\Controller\Plugin\Basket;

class ProductController extends AbstractActionController
{
    protected $productTable;
    protected $ordersTable;
    public function getProductTable()
    {
        if (!$this->productTable) {
            $sm = $this->getServiceLocator();
            $this->productTable = $sm->get('Product\Model\productTable');
        }
        return $this->productTable;
    }
    public function getOrderTable()
    {
        if (!$this->ordersTable) {
            $sm = $this->getServiceLocator();
            $this->ordersTable = $sm->get('Product\Model\OrdersTable');
        }
        return $this->ordersTable;
    }

    public function allAction()
    {
        return new ViewModel(array(
            'product' => $this->getproductTable()->fetchAll(),
        ));
    }

    public function addAction()
    {
        $form = new ProductForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $product = new Product();
            $form->setInputFilter($product->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {

                $product->exchangeArray($form->getData());
                $this->getProductTable()->saveProduct($product);


                return $this->redirect()->toRoute('product');
            }

        }
        return array('form' => $form);
    }

    public function editAction()
    {
        $id = (int)$this->params()->fromPost('id', 6);

        $product = $this->getProductTable()->getProduct($id);


        $form = new ProductForm();
        $form->bind($product);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($product->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getProductTable()->saveProduct($product);

                return $this->redirect()->toRoute('product');
            }
        }

        return array(
            'id' => $id,
            'form' => $form,
        );
    }

    public function deleteAction()
    {

        $id = $this->params()->fromRoute('id', 6);


        $request = $this->getRequest();

        if ($request->isPost()) {

            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int)$request->getPost('id');
                $this->getProductTable()->deleteProduct($id);
            }


            return $this->redirect()->toRoute('product');
        }

        return array(
            'id' => $id,
            'product' => $this->getProductTable()->getProduct($id)
        );
    }


    public function uploadFormAction()
    {
        $id = (int)$this->params()->fromPost('id');


        $form = new UploadForm('upload-form');
        $request = $this->getRequest();
        if ($request->isPost()) {

            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $form->setData($post);

            if ($form->isValid()) {
                $tempName = rand(1, 9999) . $post['image-file']['name'];
                $uploadPath = "public/imgProducts/$tempName";
                move_uploaded_file($post['image-file']['tmp_name'], $uploadPath);
                $this->getProductTable()->addImg($uploadPath, $id);


                return $this->redirect()->toRoute('product');
            }
        }
        return array('form' => $form);
    }

    public function addIntoBasketAction()
    {
        session_start();
        $basket = new Basket();
        $id = $this->params()->fromPost('productId'); //TODO set in basket
        $add = array(
            'message' => 'Your product transfer to basket',
            'success' => true,
        );
        $already = array(
            'message' => 'Your product already in to basket',
            'success' => true,
        );

        $content = $basket->getSession();
        foreach ($content as $key => $value) {
            if ($value == $id) {
                $this->response->setContent(json_encode($already));
                return $this->response;
            }
        }

        $basket->addItemIntoSession($id);
        $this->response->setContent(json_encode($add));
        return $this->response;


    }

    public function clearBasketAction()
    {
        session_start();

        $clear = 'Empty';


        unset($_SESSION['basket']);
        $this->response->setContent($clear);
        return $this->response;
    }

    public function statusBasketAction()
    {
        session_start();
        if ($_SESSION['basket'] !== NULL) {
            $content = '';
            foreach ($_SESSION['basket'] as $key => $value) {
                $content .= $value;

                if ($key >= 0 and $key != count($_SESSION['basket']) - 1) $content .= ',';
            }

            $this->response->setContent($content);
            return $this->response;
        } else {
            $content = 'Empty';
            $this->response->setContent($content);
            return $this->response;
        }
    }

    public function ordersAction()
    {
        session_start();
        if($_SESSION['basket']==NULL)
        {
            return $this->redirect()->toRoute('product');
        }
        $form = new OrdersForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();

        if ($request->isPost()) {

            $orders = new Orders();

            $form->setInputFilter($orders->getInputFilter());
            $form->setData($request->getPost());


            if ($form->isValid()) {

                $orders->exchangeArray($form->getData());

                $this->getOrderTable()->saveOrders($orders);


                return $this->redirect()->toRoute('product');
            }

        }
        return array('form' => $form);
    }
}

